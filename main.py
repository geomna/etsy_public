import configparser

import bot
import logger

logger = logger.get_logger(__name__)


def getSettings():
    logger.info("Get configuration")
    settings = {}  # dic of settings
    listings = []
    cfg = configparser.ConfigParser()
    cfg.read('config.ini')
    listings_file = 'config/' + cfg['assets']['listings']
    accounts_file = 'config/' + cfg['assets']['accounts']
    proxy_file = 'config/' + cfg['assets']['proxy']

    with open(listings_file) as f:
        tmp = f.readlines()
    tmp2 = [x.strip() for x in tmp]
    for element in tmp2:
        listings.append(element.split(';'))
    settings['listings'] = listings

    with open(accounts_file) as f:
        accounts = f.readlines()
    settings['accounts'] = [x.strip() for x in accounts]

    settings['proxy'] = bot.get_proxys(proxy_file)

    settings['cfg'] = cfg
    return settings


def main():
    logger.info("Start EtsyBot v0.03")
    settings = getSettings()
    status = bot.start(settings)
    pass


if __name__ == '__main__':
    main()
