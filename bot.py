import time
from random import getrandbits
from random import randrange

import requests
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.expected_conditions import staleness_of
from selenium.webdriver.support.ui import WebDriverWait

import logger

logger = logger.get_logger(__name__)


def random_int(min, max):
    return randrange(min, max, 1)


def random_run():
    return bool(getrandbits(1))


def document_initialised(driver):
    return driver.execute_script("return initialised")


def check_proxy_available(proxy):
    # print('Check proxy: %s' % proxy['ip'], end='')
    logger.info('Check proxy: %s' % proxy['ip'])

    proxies = {
        "http": f"http://{proxy['ip']}:{proxy['port']}",
        'https': f"http://{proxy['ip']}:{proxy['port']}"
    }

    # try:
    #     headers = {
    #         'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/87.0.4280.88 Safari/537.36'}
    #     lic = requests.get("http://ealeksandrov-ru.1gb.ru/query/23415235423451.dt", headers=headers, timeout=3).raise_for_status()
    # except requests.exceptions.RequestException as e:
    #     logger.info('Unknown error')
    #     # print(e)
    #     raise SystemExit

    try:
        # r = requests.get("http://www.httpbin.org/ip", proxies=proxies, timeout=3)
        r = requests.get("https://www.etsy.com", proxies=proxies, timeout=3)
        r.raise_for_status()
    except requests.exceptions.RequestException as e:
        logger.warning(' Error')
        return False
    if r.ok:
        logger.info(' Active')
        return True
    return False


def myFunc(e):
    return e['count']


def select_proxy(proxys):
    tmp_sort = list()
    proxy = False
    for element in proxys:
        username, password, ip, port, count = element.split(';')
        tmp = {'username': username, 'password': password, 'ip': ip, 'port': port, 'count': count}
        tmp_sort.append(tmp)
    tmp_sort.sort(key=myFunc)
    for element in tmp_sort:
        if element['count'] == '1' or element['count'] == '-1':  # skip used proxy
            pass
        elif check_proxy_available(element):
            proxy = element
            break
    if not proxy:
        logger.error('No more proxy available')
        raise SystemExit
    return proxy


def start_browser(proxy, cfg):
    # Common
    proxy_txt = f"{proxy['ip']}:{proxy['port']}"
    user_agent = cfg['assets']['UserAgent']
    logger.info('Use proxy: %s' % proxy_txt)

    # Chrome
    if cfg['browser']['Type'] == 'Chrome':
        chrome_options = webdriver.ChromeOptions()
        if cfg['browser']['proxy'] == 'True':
            chrome_options.add_argument('--proxy-server=%s' % proxy_txt)
        # chrome_options.add_argument("--incognito")
        chrome_options.add_argument(f'user-agent={user_agent}')
        chrome_options.add_argument('--window-size=%s,%s' % (cfg['browser']['Width'], cfg['browser']['Height']))
        driver = webdriver.Chrome(chrome_options=chrome_options, executable_path='assets/chromedriver.exe')
        driver.delete_all_cookies()

    return driver


def login(browser, email, password):
    action = ActionChains(browser)
    # print('Logging in - ', end='')
    logger.info('Logging in')

    # print('Button click')
    sign_in_button = browser.find_elements_by_xpath("//button[contains(.,'Sign in')]")
    if len(sign_in_button) != 0:
        action.move_to_element(sign_in_button[0]).click().perform()
        time.sleep(2)

    # print('Username enter')
    join_neu_email_field = browser.find_elements_by_id("join_neu_email_field")
    if len(join_neu_email_field) != 0:
        # action.move_to_element(join_neu_email_field[0]).click().perform()
        join_neu_email_field[0].send_keys(email)
        time.sleep(2)

    # print('Password enter')
    join_neu_password_field = browser.find_elements_by_id("join_neu_password_field")
    if len(join_neu_password_field) != 0:
        # action.move_to_element(join_neu_password_field[0]).click().perform()
        join_neu_password_field[0].send_keys(password)
        join_neu_password_field[0].send_keys(Keys.ENTER)

    # проверяем на наличие попапа
    time.sleep(5)
    popup = False
    try:
        popup = WebDriverWait(browser, timeout=5).until(lambda d: d.find_element_by_xpath(
            '//*[@id="join-neu-overlay"]/div/button'))
    except TimeoutException as e:
        popup_present = False

    # если попап есть - ожилаем действие от пользователя
    if popup:
        # logger.info('Wait for user action', end='')
        if browser.find_elements_by_id("aria-join_neu_email_field-error"):
            logger.warning('Email address or Password is invalid')
            return False
        logger.info('Wait for user action')
        while True:
            try:
                # print('*', end='')
                WebDriverWait(browser, timeout=10).until(staleness_of(popup))
                break
            except TimeoutException as e:
                pass

    second_fall = False

    try:
        # Check for logging on
        login_status2 = WebDriverWait(browser, timeout=5).until(lambda d: d.find_element_by_xpath(
            '//*[@id="gnav-header-inner"]/div[4]/nav/ul/li[3]/div/a/div[1]/span[1]/div/img'))
    except TimeoutException as e:
        second_fall = True

    if second_fall:
        logger.info(' Failed')
        return False
    else:
        logger.info(" Successful")

    return True


def c_listing_search(search_string, elements, browser):
    action = ActionChains(browser)
    logger.info(f'Search by "{search_string}" for: ({elements})')
    try:
        input_field = browser.find_elements_by_xpath('//*[@id="global-enhancements-search-query"]')  # input field
        action.move_to_element(input_field[0]).click().perform()  # hover and click into input field
        time.sleep(random_int(1, 2))
        input_field[0].clear()
        input_field[0].send_keys(search_string)
        time.sleep(random_int(1, 4))
        input_field[0].send_keys(Keys.ENTER)
    except Exception as e:
        pass
    return True


def c_pages_url(browser):
    pages = []
    elems = browser.find_elements_by_xpath(
        "//a[contains(@href, '&ref=pagination&page=')]")  # //a[contains(@href, 'jam')
    pages.append(browser.current_url)
    for elem in elems:
        if elem.get_attribute("href") not in pages:
            pages.append(elem.get_attribute("href"))
    logger.info("Total (%s) pages" % len(pages))
    return pages


def c_listings_url(pages_url, elements, browser):
    elements_found = []
    elements_count = len(elements.split(','))

    for page_url in pages_url:
        if elements_count == 0:
            break
        # скипаем загрузку первой страницы
        if page_url != pages_url[0]:
            try:
                browser.get(page_url)
            except TimeoutException as e:
                pass

        for element in elements.split(','):
            urls = browser.find_elements_by_xpath(f'//a[contains(@href, "/listing/{element}")]')
            if urls:
                for url in urls:
                    if "&organic_search_click=" in url.get_attribute('href'):
                        logger.info(f"Id: {element} [found]")
                        start_imitation(url, browser)
                        elements_count = elements_count - 1
                        elements_found.append(element)
    if len(set(elements.split(',')) ^ set(elements_found)) != 0:
        logger.warning("Id: %s [not found]" % (set(elements.split(',')) ^ set(elements_found)))

    return True


def learn_more(browser):
    logger.info(' Learn more')
    if random_run():
        while True:
            try:
                learn_more_button = browser.find_elements_by_xpath(
                    '//*[@id="product-description-content-toggle"]/div/div[2]/div[2]/button')
                break
            except TimeoutException as e:
                # print("*", end='')
                pass
        if learn_more_button:
            action = ActionChains(browser)
            action.move_to_element(learn_more_button[0]).perform()
            time.sleep(random_int(10, 15))
            action.click().perform()
            # print(". ", end='')
            return True
    else:
        logger.info(' [random skip]. ')
    return False


def show_policies(browser):
    logger.info(' Show policies')
    if random_run():
        while True:
            try:
                show_policies = browser.find_elements_by_xpath(
                    '//*[@id="shipping-variant-div"]/div/div[3]/div[1]/button')
                break
            except TimeoutException as e:
                # print("*", end='')
                pass
        if show_policies:
            action = ActionChains(browser)
            try:
                action.move_to_element(show_policies[0]).perform()
                time.sleep(random_int(1, 5))
                action.click().perform()
            except Exception as e:
                logger.warning(' Can`t open popup')
                return False
            print(". ", end='')
            return True
    else:
        logger.info(' [random skip]')
    return False


def close_policies(browser):
    logger.info('Close policies')
    while True:
        try:
            close_policies = browser.find_elements_by_xpath('//*[@id="policies-overlay"]/div/button')
            break
        except TimeoutException as e:
            # print("*", end='')
            pass
    if close_policies:
        action = ActionChains(browser)
        try:
            action.move_to_element(close_policies[0]).perform()
            time.sleep(random_int(1, 5))
            action.click().perform()
        except Exception as e:
            logger.warning(' Can`t close popup')
            return False

        print('. ', end='')
        return True
    return False


def photo_click(browser):
    count = random_int(1, 5)
    logger.info(' Slider photo (%s)', count)
    for x in range(count):
        while True:
            try:
                click_next = browser.find_elements_by_xpath(
                    '//*[@id="listing-right-column"]/div/div[1]/div[1]/div/div/div[2]/div/div[1]/button[2]')
                break
            except TimeoutException as e:
                pass
        if click_next:
            action = ActionChains(browser)
            try:
                action.move_to_element(click_next[0]).perform()
                time.sleep(random_int(10, 15))
                action.click().perform()
            except Exception as e:
                logger.warning(' Can`t slide photo')
                return False
    return True


def start_imitation(url_obj, browser):
    action = ActionChains(browser)
    original_window = browser.current_window_handle

    logger.info(' Imitation start:')

    # переход по клику на картинку, открывается в новой вкладке
    action.move_to_element(url_obj).perform()
    time.sleep(4)
    action.click().perform()

    # Loop through until we find a new window handle
    for window_handle in browser.window_handles:
        if window_handle != original_window:
            browser.switch_to.window(window_handle)
            break
    time.sleep(2)

    if photo_click(browser):
        time.sleep(random_int(15, 20))

    if learn_more(browser):
        time.sleep(random_int(15, 30))

    if show_policies(browser):
        time.sleep(random_int(5, 10))
        close_policies(browser)
        time.sleep(random_int(5, 10))

    browser.close()
    browser.switch_to.window(original_window)
    logger.info(' Imitation end.')
    return True


def circle(browser, listings, cfg):
    logger.info('-------- Start loop ----------')
    for element in listings:

        # выполнение поискового запроса
        try:
            c_listing_search(search_string=element[0], elements=element[1], browser=browser)
        except Exception as e:
            logger.error('Can`t start searching [%s]' % element)
            break
        # получение списка пагинации поисковой выдачи
        pages_url = c_pages_url(browser)

        urls_obj = c_listings_url(pages_url=pages_url, elements=element[1], browser=browser)
    logger.info('-------- End loop ----------')
    pass


def update_accounts(account, proxy, cfg):
    accounts_file = 'config/' + cfg['assets']['accounts']
    proxy_file = 'config/' + cfg['assets']['proxy']

    with open(accounts_file) as f:
        lines = f.readlines()
    accounts = [x.strip() for x in lines]

    acc = accounts[accounts.index(account)].split(';')
    acc[2] = proxy["ip"]
    acc[3] = proxy["port"]
    accounts[accounts.index(account)] = ';'.join(acc)
    with open(accounts_file, 'w') as f:
        f.write('\n'.join(accounts))

    with open(proxy_file) as f:
        lines = f.readlines()
    proxys = [x.strip() for x in lines]

    indices = [i for i, s in enumerate(proxys) if proxy['ip'] in s]
    prx = proxys[indices[0]].split(';')
    prx[4] = "1"
    proxys[indices[0]] = ';'.join(prx)
    with open(proxy_file, 'w') as f:
        f.write('\n'.join(proxys))
    # print(accounts)
    pass


def remove_update_proxy(proxy, cfg):
    proxy_file = 'config/' + cfg['assets']['proxy']

    with open(proxy_file) as f:
        lines = f.readlines()
    proxys = [x.strip() for x in lines]

    indices = [i for i, s in enumerate(proxys) if proxy['ip'] in s]
    prx = proxys[indices[0]].split(';')
    prx[4] = "-1"
    proxys[indices[0]] = ';'.join(prx)
    with open(proxy_file, 'w') as f:
        f.write('\n'.join(proxys))
    # print(accounts)

    with open(proxy_file) as f:
        lines = f.readlines()
    proxy = [x.strip() for x in lines]
    pass


def get_proxys(proxy_file):
    with open(proxy_file) as f:
        proxy = f.readlines()
    tmp = [x.strip() for x in proxy]
    return tmp


def start(settings):
    listings = settings['listings']
    accounts = settings['accounts']
    proxys = settings['proxy']
    cfg = settings['cfg']
    proxy_file = 'config/' + cfg['assets']['proxy']

    for account in accounts:
        email, password, ip, port = account.split(';')
        proxy = {'ip': ip, 'port': port}
        logger.info("Start session for: %s, trying proxy %s:%s" % (email, proxy['ip'], proxy['port']))

        if cfg['browser']['proxy'] == 'True':
            # тестируем прокси на доступность, если он привязан к учетке
            if proxy['ip'] != '0':
                if not check_proxy_available(proxy):
                    remove_update_proxy(proxy, cfg)
                    proxys = get_proxys(proxy_file)
                    proxy['ip'] = '0'

            # если прокси не привязан, то подбираем новый
            if proxy['ip'] == '0':
                proxy = select_proxy(proxys)

            update_accounts(account, proxy, cfg)

            # запускаем экземпляр браузера
            browser = start_browser(proxy, cfg=cfg)
            browser.delete_all_cookies()
            browser.set_page_load_timeout(10)
        else:
            proxy = {'ip': '0', 'port': '0'}
            browser = start_browser(proxy, cfg=cfg)
            browser.delete_all_cookies()
            browser.set_page_load_timeout(10)

        try:
            browser.get("https://www.etsy.com")
        except TimeoutException as e:
            pass

        # Авторизуемся
        if login(browser, email, password):
            circle(browser, listings, cfg)
        else:
            logger.warning('Failed to login, skip round')
            # break
        browser.quit()
    return True
